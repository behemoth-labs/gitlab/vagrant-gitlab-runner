#!/bin/bash
#
# Install Gitlab Runner.
# Based on this guide: https://docs.gitlab.com/runner/install/linux-repository.html

cat << EOF
******************************
* Gitlab Runner Installation *
******************************
EOF

export DEBIAN_FRONTEND=noninteractive

# Required packages
apt install -y curl \
        gnupg \
        apt-transport-https \
        debian-archive-keyring

# GPG Key
curl -fsSL "https://packages.gitlab.com/runner/gitlab-runner/gpgkey" | gpg --dearmor > "/usr/share/keyrings/runner_gitlab-runner-archive-keyring.gpg"

# Repository
cat << EOF > /etc/apt/sources.list.d/runner_gitlab-runner.list
# Gitlab Runner
deb [signed-by=/usr/share/keyrings/runner_gitlab-runner-archive-keyring.gpg] https://packages.gitlab.com/runner/gitlab-runner/debian/ bookworm main
deb-src [signed-by=/usr/share/keyrings/runner_gitlab-runner-archive-keyring.gpg] https://packages.gitlab.com/runner/gitlab-runner/debian/ bookworm main
EOF

# Refreshing new repositories
apt update

# Installing Gitlab Runner
apt install -y gitlab-runner

# Adding Gitlab Runner to Docker's group
usermod -aG docker gitlab-runner
