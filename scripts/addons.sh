#!/bin/bash
#
# Installing extra/required packages to be used later on

cat << EOF
**********
* Extras *
**********
EOF

export DEBIAN_FRONTEND=noninteractive

# Installing AWS CLI
apt install -y awscli
