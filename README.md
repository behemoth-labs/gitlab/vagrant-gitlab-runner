# Gitlab Runner with Vagrant

This Vagrant module will create a single VM (based on Debian 12) and install Gitlab runner into it. It will also register to your Gitlab account and unregister when destroyed.

## Requirements

I've done my testing with the following software/version, it may work with or without the exact versions mentioned (probably will not).

* Vagrant 2.3.4
    * vagrant-libvirt 0.11.2
    * vagrant-sshfs 1.3.7
* Libvirt 9.7.0
* QEMU 8.1.3

CPU, memory and disk requirement depends on the setup you've choosen. For more details consult with [this page](https://docs.gitlab.com/ee/install/requirements.html#gitlab-runner).

## Variables

Are stored in `variables.yaml`, the file is required and all fields are mandatory. Theoretically, one of the files you will need to modify in accordiance with your system. The variables' name should be self explanatory.

### Executor Type

Adding support for 'Docker-in-Docker' executor. Under `type` you can set it to `shell` or `docker`.

## Secrets

Are stored in `secrets.yaml`, this file was included in the initial commit, but ignored in future ones (according to `.gitignore`). If the file is missing for some reason, just create a new one (make sure it is ignored):

```
---
gitlab:
  token: "<YOUR SECRET HERE>"
---
```

## Running

Run with:

```
vagrant validate
vagrant up
```

## Destroying

To destroy the VM, use the command:

```
vagrant destroy
```

Vagrant will try to unregister the runner from Gitlab, if it fails, it will not continue with the destruction. In this case, comment lines 70-74 and re-run the destroy command.

## Additional Software

If you need additional software, add thier installation instruction to `scripts/addons.sh`.
